package mySrc;

import java.util.InputMismatchException;
import java.util.Scanner;

public class QueenUser {

	
	
	
	
	public static void main(String [] args){
		Scanner keyboard = new Scanner(System.in);
		int n = 0;
		int start = 0;
		Queen queen;
		
		System.out.println("Welcome to the N Queens problem. I will do my best to find a solution for you.");
		System.out.print("Tell me what how many queens you wish to place (note: this also determines the size of the board): ");
		try{
			n = keyboard.nextInt();
		}
		
		catch(InputMismatchException e){
			System.out.println("Your input needs to be NUMBER, please. Rerun the program and try again.");
		}
		
		finally{
			// TODO run methods from queen class and print out solution.
			keyboard.close();
			queen = new Queen(n);
			queen.setPossible();
			queen.makeQueen(queen.n);
			queen.procedureQueen(start, 0); //put in 0 for placing the first queen.
			queen.printSolution();
		}
	}
}
