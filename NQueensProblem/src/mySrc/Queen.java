package mySrc;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.Stack;

import mazeSrc.Application;
import mazeSrc.BackTrack;
import mazeSrc.Position;

public class Queen implements Application {

	byte[][] grid;
	int n;
	int k = 0;
	ArrayList<Integer> possible = new ArrayList<Integer>();
	ArrayList<Integer> possible2 = new ArrayList<Integer>();
	Stack<Position> qStack = new Stack<Position>();
	Queen q;
	BackTrack back;
	int qCounter = 0;

	public Queen(int num) {
		n = num;
		grid = new byte[num][num];
	}

	public void setPossible() {
		for (int i = 0; i < n; i++) {
			//possible.add(i);
		}
	}

	public void makeQueen(int num) {
		q = new Queen(num);
		back = new BackTrack(q);
	}

	// TODO create a method that tries to solve the problem
	public void procedureQueen(int num, int column) {
		k = num;
		Position pos;
		System.out.println("Hi");

		if (k == n) {
			System.out.println("I found a solution!");
		} else if (k != n) {
			for (int j = column; j < n - 1; j++) {
				System.out.println("Next round");
				pos = new Position(k, j);
				if (qCounter == n) {
					printSolution();
					System.exit(0);
				} else if (k == n - 1 && j == n - 1 && qCounter < n) {
					qCounter--;
					pos = qStack.pop();
					System.out.println("Position is: " + pos.getRow() + "," + pos.getColumn());
					grid[pos.getRow()][pos.getColumn()] = 0;
					possible.remove(possible.size()-1);
					possible2.remove(possible2.size()-1);
					if(back.tryToReachGoal(pos)){
						procedureQueen(pos.getRow(), pos.getColumn());
					} // if
				} else {
					if(back.tryToReachGoal(pos)){
						qCounter++;
						qStack.push(pos);
						possible.add(k);
						possible2.add(j);
						System.out.println("array");
						for(int p = 0; p<possible.size();p++){
							System.out.print(possible.get(p));
							System.out.println(possible2.get(p));
						} // for
						procedureQueen(k + 1, j);
					} // if
				} // else
			} // for
		} // else
	} // method procedureQueen

	public int getRows() {
		return n;
	}

	public int getColumns() {
		return n;
	}

	// TODO create a method that prints out the solution
	public void printSolution() {
		System.out.println("Your solution is: ");
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid.length; j++) {
				if (grid[i][j] == -1) {
					System.out.print("|Q|");
				}
				else{
				System.out.print("|" + grid[i][j] + "|");
				}
			}
			System.out.print("\n");
		}
	} // method printSolution

	@Override
	public boolean isOK(Position pos) {
		// TODO rewrite to check for if its zero
		if (grid[pos.getRow()][pos.getColumn()] == 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void markAsPossible(Position pos) {
		// TODO Auto-generated method stub

		int j = pos.getColumn();
		int i;

		grid[pos.getRow()][pos.getColumn()] = -1;

		// mark all the rows and column queen hits.
		for (i = 0; i < n; i++) {
			if (grid[i][pos.getColumn()] != -1) {
				grid[i][pos.getColumn()] += 1;
			}
			if (grid[pos.getRow()][i] != -1) {
				grid[pos.getRow()][i] += 1;
			}
		}

		// mark all the diagonals
		i = pos.getRow();
		while (i >= 1 && j >= 1) {
			if (grid[i - 1][j - 1] != -1) {
				grid[i - 1][j - 1] += 1;
			}

			i--;
			j--;
		}

		i = pos.getRow();
		j = pos.getColumn();

		while (i < n - 1 && j >= 1) {
			if (grid[i + 1][j - 1] != -1) {
				grid[i + 1][j - 1] += 1;
			}
			i++;
			j--;
		}

		i = pos.getRow();
		j = pos.getColumn();

		while (i < n - 1 && j < n - 1) {
			if (grid[i + 1][j + 1] != -1) {
				grid[i + 1][j + 1] += 1;
			}
			i++;
			j++;
		}

		printSolution();
		
	} // method markAsPossible

	@Override
	public boolean isGoal(Position pos) {
		// TODO Auto-generated method stub
		return getRows() == n;
	} // method isGoal

	@Override
	public void markAsDeadEnd(Position pos) {
		// TODO Auto-generated method stub
		grid[pos.getRow()][pos.getColumn()] = 0;

	} // method markAsDeadEnd

	@Override
	public Iterator<Position> iterator(Position pos) {
		// TODO Auto-generated method stub

		return new QueenIterator(pos);
	} // method iterator

	protected class QueenIterator implements Iterator<Position> {

		protected int row;
		protected int column;
		protected int numQueens = 0;

		public QueenIterator(Position pos) {
			row = pos.getRow();
			column = pos.getColumn();
			numQueens = qCounter;
		}

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return numQueens < n;
		}

		@Override
		public Position next() {
			// TODO Auto-generated method stub
			boolean foundPosition = false;
			Position nextPosition = new Position();
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					if (grid[i][j] == 0) {
						nextPosition = new Position(i, j);
						foundPosition = true;
						break;
					}
				}
				if (foundPosition) {
					break;
				}
			}

			return nextPosition;
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub
			throw new UnsupportedOperationException();
		}
	}
}
