//A program that determines if the string submitted is a palindrome.
//Takes in just one word (not a sentence).

#include <stdio.h>
#include <stdlib.h>

struct stackNode{
	char[] data;
	struct stackNode *nodePtr;
};

typedef struct stackNode StackNode;
typedef StackNode *StackNodePtr;

void push( StackNodePtr *topPtr, char info );
char pop( StackNodePtr *topPtr );
int isEmpty( StackNodePtr topPtr );
void printStack( StackNodePtr currentPtr );

int main( void ){
	StackNodePtr stackPtr = NULL;
	puts("Enter your word: ");
	scanf("%s", &data);
	char *stringPtr;
	stringPtr = data;
	char tempChar;
	
	//add all of the string to the stack
	while( *stringPtr != NULL ){
		push( &stackPtr, *stringPtr );
		stringPtr++;
	}// while
	
	while(!isEmpty( stackPtr )){
		tempChar = pop( stackPtr );
		while( *stringPtr != NULL ){
			if( *stringPtr != tempChar){
				puts("Not a palindrome
				break;
			}// if
			stringPtr++;
		}// while
	}// while
}// method main

void push( StackNodePtr *topPtr, int info ){
	StackNodePtr newPtr;
	
	newPtr = malloc( sizeof( StackNode ) );
	
	if( newPtr != NULL ){
		newPtr->data = info;
		newPtr->nextPtr = *topPtr;
		*topPtr = newPtr;
	}
	else {
		printf("%d not insterted. No memory available. \n", infor );
	}
}

char pop( StackNodePtr *topPtr ){
	StackNodePtr tempPtr;
	char popValue;
	
	tempPtr = *topPtr;
	popValue = ( *topPtr )->data;
	*topPtr = ( *topPtr )->nextPtr;
	free( tempPtr );
	return popValue;
}

int isEmpty( StackNodePtr topPtr ){
	return topPtr == NULL;
}